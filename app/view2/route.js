(function() {
    'use strict';

    // minification safe
    ngViewRoute.$inject = ['$routeProvider'];

    function ngViewRoute($routeProvider) {
        $routeProvider.when('/view2', {
            templateUrl: 'view2/view.html',
            controller: 'ViewCtrl',
            controllerAs: 'vc'
        });
    };

    angular.module('myApp.view2', ['ngRoute']).config(ngViewRoute);

})();