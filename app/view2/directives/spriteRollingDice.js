(function() {
    'use strict';

    // minification safe
    spriteRollingDice.$inject = ['viewLoaderSvc', 'Dice', 'Background', 'BottomText', 'Board'];

    function spriteRollingDice(viewLoaderSvc,Dice,Background, BottomText, Board) {
        return {
            restrict : 'EAC',
            replace : true,
            scope : {
                width: '=width',
                height: '=height'
            },
            template: "<canvas></canvas>",
            link: function (scope, element, attribute) {

                var w, h, dice, background, board, bottomText;

                drawAnimation();

                element[0].width = scope.width;
                element[0].height = scope.height;

                w = scope.width;
                h = scope.height;

                function drawAnimation() {

                    if(scope.stage) {
                        scope.stage.autoClear = true;
                        scope.stage.removeAllChildren();
                        scope.stage.update();
                    } else {
                        scope.stage = new createjs.Stage(element[0]);
                    }

                    viewLoaderSvc.getLoader().addEventListener("complete", handleComplete);
                    viewLoaderSvc.loadAssets();

                };

                function handleComplete() {
                    background = new Background({Width: w, Height: h});
                    board = new Board({ assetName: "board", scale: 1.2 });
                    dice = new Dice({assetName: "dice", scale: 1});
                    //bottomText = new BottomText({x: (w/2), y: h});

                    board.setX(w*0.1);
                    board.setY(h*0.1);

                    //dice.setX(w*0.09);
                    //dice.setY(h*0.09);
                    //dice.setX(board.getX()-(board.getX()*0.1));
                    //dice.setY(board.getY()-(board.getY()*0.1));
                    dice.setX(board.getX()-(dice.getWidth()*0.13));
                    dice.setY(board.getY()-(dice.getHeight()*0.13));

                    //bottomText.setX((w/2)-(bottomText.getWidth()/2)); bottomText.setY(h-(h*0.03));

                    background.addToStage(scope.stage);
                    board.addToStage(scope.stage);
                    dice.addToStage(scope.stage);
                    //bottomText.addToStage(scope.stage);

                    createjs.Ticker.timingMode = createjs.Ticker.RAF;
                    createjs.Ticker.addEventListener("tick", tick);

                    window.onkeydown = keydown;

                    scope.currentNumber = 1;

                    scope.$apply();

                };

                function keydown(event) {
                    if(event.keyCode === 37) {  // left
                        switch(scope.currentNumber) {
                            case 1:
                                scope.currentNumber = 2;
                                dice.playAnimation("onetwo");
                                getStep("left");
                                break;
                            case 2:
                                scope.currentNumber = 6;
                                dice.playAnimation("twosix");
                                getStep("left");
                                break;
                            case 6:
                                scope.currentNumber = 5;
                                dice.playAnimation("sixfive");
                                getStep("left");
                                break;
                            case 5:
                                scope.currentNumber = 1;
                                dice.playAnimation("fiveone");
                                getStep("left");
                                break;
                        };
                    }
                    if(event.keyCode === 39) {  // right
                        scope.originDiceX = dice.getX();
                        switch(scope.currentNumber) {
                            case 1:
                                scope.currentNumber = 5;
                                dice.playAnimation("onefive");
                                getStep("right");
                                break;
                            case 5:
                                scope.currentNumber = 6;
                                dice.playAnimation("fivesix");
                                getStep("right");
                                break;
                            case 6:
                                scope.currentNumber = 2;
                                dice.playAnimation("sixtwo");
                                getStep("right");
                                break;
                            case 2:
                                scope.currentNumber = 1;
                                dice.playAnimation("twoone");
                                getStep("right");
                                break;
                        };
                    }
                };

                function getStep(direction) {
                    if(direction === "right") {
                        scope.finalDiceX = (dice.getX()+(dice.getWidth()-(dice.getWidth()*0.3)));
                        scope.directionDice = 1;
                    }
                    if(direction === "left") {
                        scope.finalDiceX = (dice.getX()-(dice.getWidth()-(dice.getWidth()*0.3)));
                        scope.directionDice = -1;
                    }
                    scope.incrementDiceX = 1;
                };

                function tick(event) {
                    if((scope.originDiceX < scope.finalDiceX) && (scope.directionDice === 1)) {
                        //console.info(scope.originDiceX);
                        dice.setX(scope.originDiceX);
                        scope.originDiceX += scope.incrementDiceX;
                    }
                    if((scope.originDiceX > scope.finalDiceX) && (scope.directionDice === -1)) {
                        //console.info(scope.originDiceX);
                        dice.setX(scope.originDiceX);
                        scope.originDiceX -= scope.incrementDiceX;
                    }
                    scope.stage.update(event);
                };
            }
        };

    };

    myDirectives.directive('spriteRollingDice', spriteRollingDice);


})();
