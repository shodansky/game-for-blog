(function() {

    'use strict';

    function BottomText() {

        function BottomText(obj) {
            this.bottomText = new createjs.Text("< (Arrow Keys) >","20px Arial","#fff");
            this.bottomText.textBaseline = "alphabetic";
        }
        BottomText.prototype = {
            addToStage: function (stage) {
                stage.addChild(this.bottomText);
            },
            removeFromStage: function (stage) {
                stage.removeChild(this.table);
            },
            getWidth: function() {
                return this.bottomText.getBounds().width;
            },
            getHeight: function() {
                return this.bottomText.getBounds().height;
            },
            setX: function(val) {
                this.bottomText.x = val;
            },
            setY: function(val) {
                this.bottomText.y = val;
            }
        };
        return BottomText;
    };

    uiClasses.factory("BottomText", BottomText);

})();