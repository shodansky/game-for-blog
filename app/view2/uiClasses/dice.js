(function() {

    'use strict';

    // minification safe
    Dice.$inject = ['viewLoaderSvc'];

    function Dice(viewLoaderSvc) {

        function Dice(obj) {
            var Number = { 
                one: 4*16, two: (4*16)+4, three: 8*16, four: 0, five: (4*16)+12, six: (4*16)+8
            };
            var rollSpeed = 0.3;
            var spriteSheet = new createjs.SpriteSheet({
                framerate: 30,
                "images": [viewLoaderSvc.getResult(obj.assetName)],
                "frames": {"width": 46, "height": 46, "regX": 0, "regY": 0, "count": 144},
                "animations": {
                    "one": [Number.one],
                    "two": [Number.two],
                    "three": [Number.three],
                    "four": [Number.four],
                    "five": [Number.five],
                    "six": [Number.six],
                    "onetwo": [Number.one, Number.two, "two", rollSpeed],
                    "twoone": [Number.two, Number.two, "twoone3", rollSpeed],
                    "twoone3": [Number.one+3, Number.one+3, "twoone2", rollSpeed],
                    "twoone2": [Number.one+2, Number.one+2, "twoone1", rollSpeed],
                    "twoone1": [Number.one+1, Number.one+1, "one", rollSpeed],
                    "twosix": [Number.two, Number.six, "six", rollSpeed],
                    "sixtwo": [Number.six, Number.six, "sixtwo3", rollSpeed],
                    "sixtwo3": [Number.two+3, Number.two+3, "sixtwo2", rollSpeed],
                    "sixtwo2": [Number.two+2, Number.two+2, "sixtwo1", rollSpeed],
                    "sixtwo1": [Number.two+1, Number.two+1, "two", rollSpeed],
                    "sixfive": [Number.six, Number.five, "five", rollSpeed],
                    "fivesix": [Number.five, Number.five, "fivesix3", rollSpeed],
                    "fivesix3": [Number.six+3, Number.six+3, "fivesix2", rollSpeed],
                    "fivesix2": [Number.six+2, Number.six+2, "fivesix1", rollSpeed],
                    "fivesix1": [Number.six+1, Number.six+1, "six", rollSpeed],
                    "fiveone": [Number.five, Number.five+3, "one", rollSpeed],
                    "onefive": [Number.one, Number.one, "onefive3", rollSpeed],
                    "onefive3": [Number.five+3, Number.five+3, "onefive2", rollSpeed],
                    "onefive2": [Number.five+2, Number.five+2, "onefive1", rollSpeed],
                    "onefive1": [Number.five+1, Number.five+1, "five", rollSpeed],
                }
            });
            this.dice = new createjs.Sprite(spriteSheet, "one");

            this.dice.scaleX = obj.scale;
            this.dice.scaleY = obj.scale;

        };

        Dice.prototype = {
            addToStage: function (stage) {
                stage.addChild(this.dice);
            },
            removeFromStage: function (stage) {
                stage.removeChild(this.dice);
            },
            getWidth: function() {
                return this.dice.getBounds().width;
            },
            getHeight: function() {
                return this.dice.getBounds().height;
            },
            getX: function () {
                return this.dice.x;
            },
            getY: function (val) {
                return this.dice.y;
            },
            setX: function (val) {
                this.dice.x =  val;
            },
            setY: function (val) {
                this.dice.y =  val;
            },
            playAnimation: function (animation) {
                this.dice.gotoAndPlay(animation);
            }
        };

        return Dice;
    };

    uiClasses.factory("Dice", Dice);


})();