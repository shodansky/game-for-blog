(function() {

    'use strict';
    
    // minification safe
    Board.$inject = ['viewLoaderSvc'];

    function Board(viewLoaderSvc) {

        function Board(obj) {
            this.board = new createjs.Bitmap(viewLoaderSvc.getResult(obj.assetName));
            this.board.scaleX = obj.scale;
            this.board.scaleY = obj.scale;
        };

        Board.prototype = {
            addToStage: function(stage) {
                stage.addChild(this.board);
            },
            removeFromStage: function(stage) {
                stage.removeChild(this.board);
            },
            getWidth: function() {
                return this.board.getBounds().width;
            },
            getHeight: function() {
                return this.board.getBounds().height;
            },
            getX: function() {
                return this.board.x;
            },
            getY: function() {
                return this.board.y;
            },
            setX: function(val) {
                this.board.x = val;
            },
            setY: function(val) {
                this.board.y = val;
            }
        };

        return Board;

    };

    uiClasses.factory('Board', Board);

})();