(function() {
    
    'use strict';

    function Background() {
        function Background(obj) {
            this.background = new createjs.Shape();
            this.background.graphics.beginFill("#406d3b").drawRect(0, 0, obj.Width, obj.Height);
        }
        Background.prototype = {
            addToStage: function (stage) {
                 stage.addChild(this.background);
            },
            removeFromStage: function (stage) {
                stage.removeChild(this.background);
            }
        };
        return Background;
    };

    uiClasses.factory("Background", Background);

})();