(function() {

    'use strict';

    function viewLoaderSvc() {
        var manifest = [
                {src: "dice.png", id: "dice"},
                {src: "chess_board2.png", id: "board"}
        ], loader = new createjs.LoadQueue(true);

        this.getResult = function (asset) {
            return loader.getResult(asset);
        };
        this.getLoader = function () {
            return loader;
        };
        this.loadAssets = function () {
            loader.loadManifest(manifest, true, "/app/assets/");
        };
    }

    myServices.service('viewLoaderSvc', viewLoaderSvc);

})();