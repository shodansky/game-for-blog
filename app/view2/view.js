(function() {
    
    'use strict';

    function animationView($window) {
        var vm = this;
        vm.windowWidth = $window.innerWidth;
        vm.animationHeight = $window.innerHeight;
    };

    // minification safe
    animationView.$inject = ['$window'];

    angular.module('myApp.view2').controller('ViewCtrl', animationView);

})();