

(function() {

    'use strict';

     function Character(loaderSvc) {
        function Character(obj) {
            var spriteSheet = new createjs.SpriteSheet({
                framerate: 30,
                "images": [loaderSvc.getResult(obj.characterAssetName)],
                //"frames": {"width": 165, "height": 292, "regX": 82, "regY": 0, "count": 64},
                "frames": {"width": 165, "height": 292, "regX": 0, "regY": 0, "count": 64},
                // define two animations, run (loops, 1.5x speed) and jump (returns to run):
                "animations": {
                    "run": [0, 25, "run", 1.5],
                    "jump": [26, 63, "run"]
                }
            });
            this.grant = new createjs.Sprite(spriteSheet, "run");
            this.grant.y = obj.y;
            this.grant.scaleX = 0.4;
            this.grant.scaleY = 0.4;
        }
        Character.prototype = {
            addToStage: function (stage) {
                stage.addChild(this.grant);
            },
            removeFromStage: function (stage) {
                stage.removeChild(this.grant);
            },
            getWidth: function () {
                return this.grant.getBounds().width * this.grant.scaleX;
            },
            getX: function () {
                return this.grant.x;
            },
            setX: function (val) {
                this.grant.x =  val;
            },
            playAnimation: function (animation) {
                this.grant.gotoAndPlay(animation);
            }
        };
        return (Character);
    };

    Character.$inject = ['loaderSvc'];

    uiClasses.factory("Character", Character);

})();


